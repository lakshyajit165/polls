import React, { Component } from 'react';
import { Icon } from 'antd';

class RouteNotFound extends Component {
    render() {
        
        return (
            <div style={{ paddingTop: '30px', paddingBottom: '30px', textAlign: 'center'}}>
                <div style={{ textAlign: 'center' }}>
                <Icon type="exclamation-circle" style={{ fontSize: '30px'}}/>  
                </div>
                <h1>The route you requested is not accesssible!</h1>
            </div>
        );
    }
}

export default RouteNotFound;