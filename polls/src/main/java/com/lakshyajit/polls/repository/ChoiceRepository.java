package com.lakshyajit.polls.repository;

import com.lakshyajit.polls.model.Choice;
import com.lakshyajit.polls.model.Poll;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface ChoiceRepository extends JpaRepository<Choice, Long> {

//    @Query("DELETE FROM choices WHERE poll_id = :pollId")
//    List<Choice> deleteByPollId(@Param("pollId") Long pollId);
    void deleteByPollId(Long Id);
}