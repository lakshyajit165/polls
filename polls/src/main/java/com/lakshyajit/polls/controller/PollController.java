package com.lakshyajit.polls.controller;

import com.lakshyajit.polls.model.*;
import com.lakshyajit.polls.payload.*;
import com.lakshyajit.polls.repository.PollRepository;
import com.lakshyajit.polls.repository.UserRepository;
import com.lakshyajit.polls.repository.VoteRepository;
import com.lakshyajit.polls.security.CurrentUser;
import com.lakshyajit.polls.security.UserPrincipal;
import com.lakshyajit.polls.service.PollService;
import com.lakshyajit.polls.util.AppConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/polls")
public class PollController {



    private PollService pollService;

    private static final Logger logger = LoggerFactory.getLogger(PollController.class);

    @Autowired
    public PollController(PollService pollService) {
        this.pollService = pollService;
    }

    @GetMapping
    public PagedResponse<PollResponse> getPolls(@CurrentUser UserPrincipal currentUser,
                                                @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                                @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {

        return pollService.getAllPolls(currentUser, page, size);

    }

    @PostMapping
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createPoll(@Valid @RequestBody PollRequest pollRequest) {

        Poll poll = pollService.createPoll(pollRequest);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{pollId}")
                .buildAndExpand(poll.getId()).toUri();


        return ResponseEntity.created(location)
                .body(new ApiResponse(true, "Poll Created Successfully!"));

    }

    @GetMapping("/{pollId}")
    public PollResponse getPollById(@CurrentUser UserPrincipal currentUser,
                                    @PathVariable Long pollId) {
        return pollService.getPollById(pollId, currentUser);
    }

    @PostMapping("/{pollId}/votes")
    @PreAuthorize("hasRole('USER')")
    public PollResponse castVote(@CurrentUser UserPrincipal currentUser,
                                 @PathVariable Long pollId,
                                 @Valid @RequestBody VoteRequest voteRequest) {

        return pollService.castVoteAndGetUpdatedPoll(pollId, voteRequest, currentUser);

    }

    @DeleteMapping("/{pollId}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> deletePoll(@PathVariable Long pollId){
        if(pollService.deletePoll(pollId).equals("Poll deleted successfully!")){
            return ResponseEntity.ok()
                    .body(new ApiResponse(true, "Poll Created Successfully!"));
        }else{
            return ResponseEntity.status(400)
                    .body(new ApiResponse(false, "An error occurred!"));
        }

    }

}
